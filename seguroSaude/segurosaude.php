<!DOCTYPE html>
<html lang="pt-br">

<head>
    <title>Rodrigo Carvalho</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="css.css">
</head>

<body>
    <div class="container center-block">
        <div class="col-md-6 col-md-push-3 col-xs-12">
            <h1>Seguro Sa&uacute;de</h1>
            <p>Formulário de cadastro para seguro</p>
            <form role="form" id="frm1" name="frm1" method="post" action="seguro.php">
                <div class="form-group col-md-12 col-sm-12 col-xs-12" style="padding-left: 0 !important;" <input class="form-control" type="text" required="required" id="name" name="name" placeholder="Digite seu nome">
                </div>
                <div class="form-group col-md-12 col-sm-12 col-xs-12" style="padding-left!important;">
                    <select class="form-control" class="form-control" name="faixa" id="faixa" required="required">
<option value="" disabled selected>Faixa etária</option>
<option value="0">20 ou menos</option>
<option value="1">21~30</option>
<option value="2">31~40</option>
<option value="3">41~50</option>
<option value="4">51~65</option>
<option value="5">66 ou mais</option>
</select>
                </div>
                <div class="col-xs-12 col-md-12">
                    <label class="radio-inline">
<input type="radio" required name="doenca" value="sim">Portador de doen&ccedil;a pr&eacute;via
</label>
                    <label class="radio-inline">
<input type="radio" required name="doenca" value="nao">N&atilde;o
</label>
                </div>
                <div class="form-group col-md-3 col-sm-12 col-xs-12" style="padding-left: 0 !important;">
                    <button type="submit" class="btn btn-default">Enviar</button>
                </div>
            </form>
        </div>
    </div>
</body>

</html>
