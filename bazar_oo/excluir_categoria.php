<?php
  require_once( "./comum.php");
	require_once( BASE_DIR . "/classes/Categoria.php");

  $id = $_POST["idCategoria"];

  $categoria = Categoria::findById( $id);

  $categoria->excluir();

  Header("Location: categoria.php");
