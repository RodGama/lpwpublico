<?php
require_once( BASE_DIR . "/classes/Banco.php");

trait CategoriaDao
{
  public static function rowMapper($idCategoria, $descricao, $taxa)
  {
    return new Categoria( $idCategoria, $descricao, $taxa);
  }

  public static function findAll()
  {
      $pdo = Banco::obterConexao();
      $statement = $pdo->prepare("SELECT idCategoria,descricao,taxa FROM Categoria");
      $statement->execute();
      /*
      return $statement->fetchAll( PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE,
      "Categoria", array( 'xxxx', 'xxx', 'xxx') );
      */

      return $statement->fetchAll( PDO::FETCH_FUNC, "CategoriaDao::rowMapper" );
  }
  
 public static function inserir(Categoria $categoria)
  {
	$pdo = Banco::obterConexao();
	$statement = $pdo->query("SELECT max(idCategoria) as lastId FROM Categoria");

	$ultimoRegistro = $statement->fetch();

	$ultimo_id = $ultimoRegistro["lastId"];

	if( $ultimo_id == null ){
		$id = 1;
	}	
	else{
		$id = $ultimo_id + 1;
	}	

	$inserir = $pdo->prepare("insert into Categoria (idCategoria, descricao, taxa) values (:id, :descricao, :taxa)");
	$inserir->bindParam( ":id", $id, PDO::PARAM_INT);
	$inserir->bindParam( ":descricao", $categoria->getDescricao(), PDO::PARAM_STR);
	$inserir->bindParam( ":taxa", str_replace( ',','.', $categoria->getTaxa() ), PDO::PARAM_STR);
	return $inserir->execute();
  }
  
}
