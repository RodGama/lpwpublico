<?php
  validaFormulario();

  function validaFormulario(){
      $erros = "";

      if (!isset($_POST["nome"]))
          $erros .= "Digite o nome<br>";

      if (!isset($_POST["usuario"]))
          $erros .= "Digite um usuario<br>";

      if (!isset($_POST["senha"]))
          $erros .= "Digite uma senha<br>";
      elseif(strlen($_POST["senha"]) < 6)
          $erros .= "Digite uma senha com no minimo 6 digitos<br>";

      if (!isset($_POST["senha2"]))
          $erros .= "Digite a senha nomavente<br>";
      elseif(($_POST["senha"]) != $_POST["senha2"])
          $erros = "Digite a senha igual<br>";

      if(!isset($_POST["email"]))
          $erros .= "Digite o Email<br>";
      elseif(strlen($_POST["email"]) < 8)
          $erros = "Digite uma senha com no minimo 8 digitos<br>";

      if(!isset($_POST["cep"]))
          $erros .= "Digite o CEP<br>";
      elseif(strlen($_POST["cep"]) < 8)
          $erros = "Cep deve contar apenas 8 digitos<br>";

      if(!isset($_POST["tel"]))
          $erros .= "Digite o Tel<br>";

      if ( strlen($erros) > 0)
        return require $erros;
      else
      {
          session_cache_expire(30);
          session_start();
          $_SESSION["usuName"] = $_POST["usuario"];
          $_SESSION["logado"]  = "S";

          $_arrTempLogin[$_POST["usuario"]] = $_POST["senha"];

          header('Location: index.php');
      }
  }
?>
