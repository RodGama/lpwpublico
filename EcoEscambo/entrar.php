<?php include_once "head.php" ?>
<div class="container">
    <h2>Entrar no EcoEscambo</h2>
    <form class="form-horizontal" role="form" action="login.php" method="post">
        <div class="form-group">
            <label class="control-label col-sm-2">Usuario</label>
            <div class="col-sm-10">
                <input type="text" name="usuario" class="form-control input-sm" required placeholder="usuario">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">Senha</label>
            <div class="col-sm-10">
                <input type="password" name="senha" class="form-control input-sm" required placeholder="senha">
            </div>
        </div>
        <div class="form-group text-center">
            <button type="submit" value="1" class="btn btn-success">
                <span class="glyphicon glyphicon-log-in"></span>
                Entrar
            </button>
        </div>
    </form>
</div>
