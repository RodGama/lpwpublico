<?php
    session_start();
    require_once "saudacao.php";
?>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">
                <img alt="Brand" src="imagens/logo.jpg" style="width: 25px;">
            </a>
        </div>
        <div class="collapse navbar-collapse">
            <p class="navbar-text"><?= $saudacao ?></p>
            <ul class="nav navbar-nav navbar-right">
                <?php
                 if ($_SESSION["logado"] == "S")
                 {
                    echo '
                        <li>
                            <a href="sair.php">
                                <span class="glyphicon glyphicon-off"></span>
                                Logoff, '.$_SESSION["usuName"].'
                            </a>
                        </li>
                    ';
                 }
                else
                {
                    echo'
                    <li>
                        <a href="entrar.php">
                            <span class="glyphicon glyphicon-log-in"></span>
                            Login
                        </a>
                    </li>
                    <li>
                        <a href="registrar.php">
                            <span class="glyphicon glyphicon-plus"></span>
                            New
                        </a>
                    </li>
                    ';
                }
                ?>

            </ul>
        </div>
    </div>
</nav>
