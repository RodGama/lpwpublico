<?php
    session_start();

    if($_SESSION["logado"] == "S")
    {
        session_destroy();
    }

    include_once "head.php";
?>
<div class="container">
    <div class="alert alert-success">Voc� saiu, volte sempre.</div>
    <a href="index.php">Inicio</a>
</div>
