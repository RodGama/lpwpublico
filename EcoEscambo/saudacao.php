<?php
	// pegando a hora
	$hr = date(" H ");

	// boas vindas
	$saudacao = "EcoEscambo deseja ";

	// verificando a saudacao
	if( ($hr >= 12) && ($hr < 18) )
		$saudacao .= "Boa tarde!";
	elseif( ($hr >= 0) && ($hr < 12) )
		$saudacao .= "Bom dia!";
	else 
		$saudacao .= "Boa noite!";
?>