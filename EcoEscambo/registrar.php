<?php include_once "head.php" ?>

<div class="container">
    <h2>Registrar no EcoEscambo</h2>
    <form class="form-horizontal" role="form" action="validar_registro.php" method="post">
        <div class="form-group">
            <label class="control-label col-sm-2">Nome</label>
            <div class="col-sm-10">
                <input type="text" name="nome" class="form-control input-sm" required placeholder="Nome completo">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">Usuario</label>
            <div class="col-sm-10">
                <input type="text" name="usuario" class="form-control input-sm" required placeholder="usuario">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">Senha</label>
            <div class="col-sm-10">
                <input type="password" name="senha" class="form-control input-sm" required placeholder="senha">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">Confirmar Senha</label>
            <div class="col-sm-10">
                <input type="password" name="senha2" class="form-control input-sm" required placeholder="Confirmar senha">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-2">Email</label>
            <div class="col-sm-10">
                <input type="email" name="email" class="form-control input-sm" required placeholder="email">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">CEP</label>
            <div class="col-sm-10">
                <input type="text" name="cep" class="form-control input-sm" required placeholder="CEP">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">Telefone</label>
            <div class="col-sm-10">
                <input type="text" name="tel" class="form-control input-sm" required placeholder="Telefone">
            </div>
        </div>

        <div class="form-group text-center">
            <button type="submit" value="1" class="btn btn-success">
                <span class="glyphicon glyphicon-plus-sign"></span>
                Registrar
            </button>
            <button type="reset" value="1" class="btn btn-danger">
                <span class="glyphicon glyphicon-trash"></span>
                Limpar
            </button>
            <a href="index.php" class="btn btn-primary">
                <span class="glyphicon glyphicon-home"></span>
                Inicio
            </a>
        </div>
    </form>
    <?php include_once "validar_registro.php" ?>
    <?php
      if (isset($erros))
      {
          echo '<div class="alert alert-danger">'.$erros.'</div>';
      }
    ?>
</div>
