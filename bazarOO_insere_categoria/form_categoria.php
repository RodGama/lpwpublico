<?php
	session_start();
	if( !isset( $_SESSION["usuario"] ) )
	{
		Header("location: inicio.php");
	}
?>
<html>
<head>
	<meta charset="UTF-8" />
	<title>Bazar Tem Tudo</title>
</head>
<body>

	<?php require_once("cabecalho.inc"); ?>

	<div id="corpo">

		<form action="inserir_categoria.php" name="formularioAdicao" onsubmit="return verificaFormulario(this)" method="post">
			<span>Descrição:</span><input type="text" id="descricao" name="descricao" maxlength="40">
			<br/>
			<span>Taxa:</span><input type="text" name="taxa" maxlength="6">

			<button type="submit">Enviar</button>
		

		</form>

	</div>

	<?php require_once("rodape.inc"); ?>
<script>
    function verificaFormulario(){
     var  descricao = document.getElementById("descricao").value;
        if((descricao.length) > 6 && (descricao[0] === descricao[0].toUpperCase())){
            document.formularioAdicao.submit();
        }
       window.alert("Descricao deve ter 6 ou mais digitos e começar com letra maiuscula");
        document.formularioAdicao.focus();
      	return 0; 
    ;
    </script>
</body>
</html>
