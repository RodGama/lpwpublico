<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<title>Rodrigo Carvalho</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	</head>
	<body>
		<h3>Contador de 1 a 100</h3>
		<div class="col-md-12 col-xs-12">
			<ul>
				<?php 
					for ( $i=0; $i<=100; $i++){
					echo "<li> $i </li>";
				}
				?>
			</ul>
		</div>
	</body>
</html>